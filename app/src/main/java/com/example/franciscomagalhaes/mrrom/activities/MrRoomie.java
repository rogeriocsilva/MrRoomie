package com.example.franciscomagalhaes.mrrom.activities;

import android.app.Application;

import com.parse.Parse;
import com.parse.ParseObject;

import com.example.franciscomagalhaes.mrrom.classes.Comunidade;
import com.example.franciscomagalhaes.mrrom.classes.Despesa;
import com.example.franciscomagalhaes.mrrom.classes.Tarefa;



/**
 * Created by franciscomagalhaes on 11/11/15.
 */
public class MrRoomie extends Application {

    @Override
    public void onCreate() {
        ParseObject.registerSubclass(Despesa.class);
        ParseObject.registerSubclass(Tarefa.class);
        ParseObject.registerSubclass(Comunidade.class);
        Parse.initialize(this, "4UMl81d6GnkVSyuERgMjFIGG8fvy7ZULk2XpPCJY", "LhYEiCK4SQ6Fi81KKT1zrCf4U6zYxHLxAotanFCY");
    }


}