package com.example.franciscomagalhaes.mrrom.activities;

/**
 * Created by franciscomagalhaes on 11/11/15.
 */

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.parse.ParseUser;


public class DispatchActivity extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        if (ParseUser.getCurrentUser() != null) {
            ParseUser user = ParseUser.getCurrentUser();
            String comu = user.getString("ComunidadeLogada");

            if (!comu.equals("none")) {
                startActivity(new Intent(this, MainActivity_Comunidade.class));
            }
            else {
                startActivity(new Intent(this, LoginComunidadeActivity.class));
            }
        } else {
            startActivity(new Intent(this, LogInActivity.class));
        }
    }


}
