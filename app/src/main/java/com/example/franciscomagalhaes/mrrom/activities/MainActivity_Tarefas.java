package com.example.franciscomagalhaes.mrrom.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.example.franciscomagalhaes.mrrom.R;
import com.example.franciscomagalhaes.mrrom.fragments.TarefasComunidadeFragment;
import com.example.franciscomagalhaes.mrrom.fragments.TarefasPessoalFragment;

import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SaveCallback;


import java.util.ArrayList;
import java.util.List;

public class MainActivity_Tarefas extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private String nomeCom;
    public View drawerHeader;
    public ViewPager viewPager;
    private TabLayout tabLayout;
    public ViewPagerAdapter adapter;
    View.OnClickListener mOnClickListener;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", mOnClickListener).show();
//            }
//        });

        mOnClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (tabLayout.getSelectedTabPosition() == 0) {

                }
            }
        };

        viewPager = (ViewPager) findViewById(R.id.viewpager);

        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);



        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();


        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        drawerHeader = navigationView.getHeaderView(0);
        TextView userNameNav = (TextView) drawerHeader.findViewById(R.id.userNameNav);
        userNameNav.setText(ParseUser.getCurrentUser().getUsername());
        TextView comNameNav = (TextView) drawerHeader.findViewById(R.id.comNameNav);
        comNameNav.setText(ParseUser.getCurrentUser().getString("ComunidadeLogada"));


        getSupportActionBar().setTitle("Tarefas");


        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


    }

    void showToast(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }




    private void setupViewPager(ViewPager viewPager) { //desire = 1 3tabs, desire = 2 1 tab
        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(new TarefasPessoalFragment(), "Pessoal");
        adapter.addFrag(new TarefasComunidadeFragment(), "Comunidade");

        viewPager.setAdapter(adapter);

    }





    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }



    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_tarefas) {
            Intent intent = new Intent(this, MainActivity_Tarefas.class);
            if(intent != null) {
                startActivity(intent);
            }
        } else if (id == R.id.nav_despesas) {
            Intent intent = new Intent(this, MainActivity_Despesas.class);
            if(intent != null) {
                startActivity(intent);
            }
        } else if (id == R.id.nav_comunidade) {
            Intent intent = new Intent(this, MainActivity_Comunidade.class);
            if(intent != null) {
                startActivity(intent);
            }
        } else if (id == R.id.nav_aboutus) {
            Intent intent = new Intent(this, AboutUsActivity.class);
            if(intent != null) {
                startActivity(intent);
            }

        } else if (id == R.id.ic_menu_logout_com) {
            final ProgressDialog dlg = new ProgressDialog(MainActivity_Tarefas.this);
            dlg.setTitle("A fazer logout.");
            dlg.setMessage("Por favor, espere...");
            dlg.show();
            logoutFromCom(ParseUser.getCurrentUser().getUsername());
            dlg.dismiss();
            Intent intent = new Intent(this, DispatchActivity.class);
            if(intent != null) {
                startActivity(intent);
            }
        } else if (id == R.id.ic_menu_logout_MrRoomie) {
            final ProgressDialog dlg = new ProgressDialog(MainActivity_Tarefas.this);
            dlg.setTitle("A fazer logout.");
            dlg.setMessage("Por favor, espere...");
            dlg.show();
            logoutFromCom(ParseUser.getCurrentUser().getUsername());
            ParseUser.logOut();
            dlg.dismiss();
            Intent intent = new Intent(this, DispatchActivity.class);
            startActivity(intent);
        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void logoutFromCom(String user) {
        ParseUser cur = ParseUser.getCurrentUser();
        cur.put("ComunidadeLogada", "none");
        cur.saveInBackground();

    }
}
