package com.example.franciscomagalhaes.mrrom.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.franciscomagalhaes.mrrom.R;
import com.example.franciscomagalhaes.mrrom.classes.Tarefa;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.InputMismatchException;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;


public class TarefasComunidadeFragment extends Fragment {
    CardViewAdapter adapter;
    ArrayList<Tarefa> listaRecycler;
    ArrayList<String> listaUsersTarefa;
    RecyclerView recyclerView;
    OnItemTouchListener itemTouchListener;
    HashMap<String, Integer> hm;

    public TarefasComunidadeFragment() {}



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d("test", "onCreateView: ");
        View view = inflater.inflate(R.layout.tarefas_pessoal_fragment, container, false);

        listaRecycler = new ArrayList<Tarefa>();


        final FrameLayout frameLayout = (FrameLayout) view.findViewById(R.id.bg);

        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity().getBaseContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);

        populateRecycler();

        itemTouchListener = new OnItemTouchListener() {
            @Override
            public void onCardViewTap(View view, int position) {
            }

            @Override
            public void onButtonRemoveClick(View view, int position) {
                removeFromParse(listaRecycler.get(position));
                removePos(position);
            }

            @Override
            public void onButtonDoneClick(View view, int position) {
                if(listaRecycler.get(position).getUsers().contains(ParseUser.getCurrentUser().getUsername())) {
                    if (listaRecycler.get(position).getDone()) {
                        ArrayList<String> aux;
                        aux = listaRecycler.get(position).getUsers();
                        aux.add(ParseUser.getCurrentUser().getUsername());
                        listaRecycler.get(position).setUsers(aux);
                        listaRecycler.get(position).setDone(false);
                        setDoneParse(listaRecycler.get(position), false);

                    } else {
                        ArrayList<String> aux;
                        aux = listaRecycler.get(position).getUsers();
                        aux.remove(ParseUser.getCurrentUser().getUsername());
                        listaRecycler.get(position).setUsers(aux);
                        listaRecycler.get(position).setDone(true);
                        setDoneParse(listaRecycler.get(position), true);
                    }
                }
                adapter.notifyItemChanged(position);

            }

        };

        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.addTarefa);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                View v = (LayoutInflater.from(getActivity())).inflate(R.layout.user_input, null);

                AlertDialog.Builder alertBuilder = new AlertDialog.Builder(getActivity());
                alertBuilder.setView(v);
                final EditText userInputDesc = (EditText) v.findViewById(R.id.userinputDesc);
                final EditText numUsers = (EditText) v.findViewById(R.id.userinputQuantia);
                numUsers.setHint("Número de pessoas");

                alertBuilder.setCancelable(true)
                        .setPositiveButton("Adicionar!", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if ((userInputDesc.getText().toString().length() > 0 && numberIsOk(numUsers.getText().toString()))) {
                                    ParseQuery<ParseObject> query = ParseQuery.getQuery("Comunidade");
                                    query.whereEqualTo("Nome", ParseUser.getCurrentUser().getString("ComunidadeLogada"));
                                    query.getFirstInBackground(new GetCallback<ParseObject>() {
                                        @Override
                                        public void done(ParseObject parseObject, ParseException e) {
                                            Integer tamanhoLista = parseObject.getList("Users").size();
                                            if (Integer.parseInt(numUsers.getText().toString()) <= tamanhoLista) {
                                                final Tarefa novaTarefa = new Tarefa();
                                                novaTarefa.put("Autor", ParseUser.getCurrentUser().getUsername());
                                                novaTarefa.put("Descricao", userInputDesc.getText().toString());

                                                final ArrayList<String> aux = new ArrayList<String>();
                                                generateListPriority();
                                                for (int i = 0; i < Integer.parseInt(numUsers.getText().toString()); i++) {
                                                    aux.add(listaUsersTarefa.get(i));
                                                }

                                                novaTarefa.put("Users", aux);
                                                novaTarefa.put("Done", false);
                                                novaTarefa.put("Comunidade", ParseUser.getCurrentUser().getString("ComunidadeLogada"));
                                                novaTarefa.saveInBackground(new SaveCallback() {
                                                    @Override
                                                    public void done(ParseException e) {
                                                        addToRecycler(new Tarefa(ParseUser.getCurrentUser().getUsername(), userInputDesc.getText().toString(), false, novaTarefa.getObjectId(), aux));
                                                        Toast.makeText(getContext(), "Tarefa Salva!", Toast.LENGTH_SHORT).show();
                                                    }
                                                });
                                            } else {
                                                Toast.makeText(getContext(), "A comunidade não tem assim tantos elementos!", Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    });
                                } else {
                                    Toast.makeText(getContext(), "Não inseriu uma tarefa válida!", Toast.LENGTH_SHORT).show();
                                }


                            }
                        });

                Dialog dialog = alertBuilder.create();
                dialog.show();


            }
        });

        return view;
    }

    public boolean numberIsOk(String str) {
        try {
            Integer d = Integer.parseInt(str);
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }

    public void setDoneParse(final Tarefa tarefa, final Boolean done) {
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Tarefa");
        query.getInBackground(tarefa.getID(), new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject parseObject, ParseException e) {
                ArrayList<String> aux = new ArrayList<String>();
                aux.add(ParseUser.getCurrentUser().getUsername());
                List lista = parseObject.getList("Users");
                ParseUser curUser = ParseUser.getCurrentUser();
                if (done) {
                    lista.remove(ParseUser.getCurrentUser().getUsername());
                    if (lista.size() == 0) {
                        parseObject.put("Done", true);
                    }
                    curUser.put("TarefasNumber", curUser.getNumber("TarefasNumber").intValue() + 1);
                } else {
                    lista.add(ParseUser.getCurrentUser().getUsername());
                    curUser.put("TarefasNumber", curUser.getNumber("TarefasNumber").intValue() - 1);
                }
                parseObject.put("Users", lista);
                curUser.saveInBackground();
                parseObject.saveInBackground();
            }
        });



    }



    public void generateListPriority() {
        Log.d("debug", "ot");
        listaUsersTarefa =  new ArrayList<>();
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Comunidade");
        query.whereEqualTo("Nome", ParseUser.getCurrentUser().getString("ComunidadeLogada"));
        List<ParseObject> list = null;
        try {
            list = query.find();
            ParseObject comunidade = list.get(0);
            ArrayList<String> users = (ArrayList<String>) comunidade.get("Users");
            Collections.shuffle(users);
            ParseQuery<ParseObject> queryTarefas = ParseQuery.getQuery("Tarefa");
            ;
            queryTarefas.whereEqualTo("Comunidade", ParseUser.getCurrentUser().getString("ComunidadeLogada"));
            List<ParseObject> list1 = null;
            try {
                list1 = queryTarefas.find();

            } catch (ParseException e1) {
                Toast.makeText(getContext(), e1.toString(), Toast.LENGTH_SHORT).show();
            }

            hm = new HashMap<>();
            for (String obj : users) {
                int aux = 0;
                for (ParseObject tarefa : list1) {
                    if (tarefa.getList("Users").contains(obj)) {
                        aux++;
                    }
                }
                hm.put(obj, aux);
            }
            listaUsersTarefa = sortByValues(hm);

        } catch (ParseException e) {
            Toast.makeText(getContext(), e.toString(), Toast.LENGTH_SHORT).show();
        }


    }



    public static <K extends Comparable,V extends Comparable> ArrayList<String> sortByValues(HashMap<K,V> map){
        List<Map.Entry<K,V>> entries = new LinkedList<Map.Entry<K,V>>(map.entrySet());

        Collections.shuffle(entries);

        Collections.sort(entries, new Comparator<Map.Entry<K,V>>() {

            @Override
            public int compare(Map.Entry<K, V> o1, Map.Entry<K, V> o2) {
                return o1.getValue().compareTo(o2.getValue());
            }
        });


        //LinkedHashMap will keep the keys in the order they are inserted
        //which is currently sorted on natural ordering
        ArrayList<String> sortedMap = new ArrayList<String>();


        for(Map.Entry<K,V> entry: entries){
            sortedMap.add((String) entry.getKey());
        }

        return sortedMap;
    }



    public void removeFromParse(Tarefa tarefa) {
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Tarefa");
        query.getInBackground(tarefa.getID(), new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject parseObject, ParseException e) {
                parseObject.deleteInBackground();
            }
        });
    }

    public void populateRecycler() {
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Tarefa");
        query.whereEqualTo("Comunidade", ParseUser.getCurrentUser().getString("ComunidadeLogada"));
        query.addAscendingOrder("Done");
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                if (e == null) {
                    for (ParseObject obj : list) {
                        List auxUsers = obj.getList("Users");
                        ArrayList<String> aux = new ArrayList<String>();
                        for (Object obje :auxUsers) {
                            aux.add(obje.toString());
                        }
                        Tarefa novaTarefa = new Tarefa(obj.getString("Autor"), obj.getString("Descricao"), obj.getBoolean("Done"), obj.getObjectId(), aux);
                        listaRecycler.add(novaTarefa);
                    }

                    if (listaRecycler.size() != 0) {
                        adapter = new CardViewAdapter(listaRecycler, itemTouchListener);
                        recyclerView.setAdapter(adapter);
                    }

                } else {
                    Toast.makeText(getContext(), e.toString(), Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    public void addToRecycler(Tarefa tarefa) {

        if (listaRecycler.size() == 0) {
            adapter = new CardViewAdapter(listaRecycler, itemTouchListener);
            recyclerView.setAdapter(adapter);
        }

        listaRecycler.add(0, tarefa);

        adapter.notifyItemInserted(0);
    }

    public void removePos(int position) {
        listaRecycler.remove(position);
        recyclerView.removeViewAt(position);
        adapter.notifyItemRemoved(position);
        adapter.notifyItemRangeChanged(position, listaRecycler.size());
    }

    /**
     * Interface for the touch events in each item
     */
    public interface OnItemTouchListener {
        /**
         * Callback invoked when the user Taps one of the RecyclerView items
         *
         * @param view     the CardView touched
         * @param position the index of the item touched in the RecyclerView
         */
        public void onCardViewTap(View view, int position);

        /**
         * Callback invoked when the ButtonRemove of an item is touched
         *
         * @param view     the Button touched
         * @param position the index of the item touched in the RecyclerView
         */


        public void onButtonRemoveClick(View view, int position);

        public void onButtonDoneClick(View view, int position);

    }

    /**
     * A simple adapter that loads a CardView layout with one TextView and two Buttons, and
     * listens to clicks on the Buttons or on the CardView
     */
    public class CardViewAdapter extends RecyclerView.Adapter<CardViewAdapter.ViewHolder> {
        private List<Tarefa> mTarefas;
        private OnItemTouchListener onItemTouchListener;

        public CardViewAdapter(List<Tarefa> mTarefas, OnItemTouchListener onItemTouchListener) {
            this.mTarefas = mTarefas;
            this.onItemTouchListener = onItemTouchListener;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.tarefa_comunidade_recycler_view_card_item, viewGroup, false);
            return new ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(ViewHolder viewHolder, int i) {
            Tarefa tar = mTarefas.get(i);
            viewHolder.desc.setText(tar.getDesc());
            String result;
            if(tar.getUsers().size() == 0) {
                viewHolder.desc.setPaintFlags(viewHolder.desc.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                result = "Tarefa completada";
            }
            else {
                String txt = "";
                for (String user : tar.getUsers()) {
                    txt += user + ", ";
                }
                result = txt.substring(0, txt.length() - 2);
            }
            viewHolder.users.setText(result);

            if(tar.getUsers().contains(ParseUser.getCurrentUser().getUsername())){
                viewHolder.done.setText("Marcar Concluído!");
                viewHolder.done.setVisibility(View.VISIBLE);
            }
            else{
                viewHolder.done.setVisibility(View.GONE);
            }


        }

        @Override
        public int getItemCount() {
            return mTarefas == null ? 0 : mTarefas.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            private TextView desc, users;
            private Button done;
            private ImageView removeItem;

            public ViewHolder(View itemView) {
                super(itemView);
                desc = (TextView) itemView.findViewById(R.id.descricao);
                done = (Button) itemView.findViewById(R.id.done);
                users = (TextView) itemView.findViewById(R.id.users);
                removeItem = (ImageView) itemView.findViewById(R.id.removeItem);

                removeItem.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onItemTouchListener.onButtonRemoveClick(v, getPosition());
                    }
                });


                done.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onItemTouchListener.onButtonDoneClick(v, getPosition());

                    }
                });

                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onItemTouchListener.onCardViewTap(v, getPosition());

                    }
                });
            }
        }
    }
}