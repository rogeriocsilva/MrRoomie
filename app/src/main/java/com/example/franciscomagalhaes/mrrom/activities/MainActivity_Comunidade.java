package com.example.franciscomagalhaes.mrrom.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.franciscomagalhaes.mrrom.R;
import com.example.franciscomagalhaes.mrrom.fragments.ComunidadeMainFragment;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity_Comunidade extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private String nomeCom;
    public View drawerHeader;
    public ViewPager viewPager;
    private TabLayout tabLayout;
    public ViewPagerAdapter adapter;
    View.OnClickListener mOnClickListener;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_comunidade);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        nomeCom = ParseUser.getCurrentUser().getString("ComunidadeLogada");

        viewPager = (ViewPager) findViewById(R.id.viewpager);

        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        if(getSupportActionBar() != null) {
            getSupportActionBar().setTitle("Comunidade");
        }

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        drawerHeader = navigationView.getHeaderView(0);
        TextView userNameNav = (TextView) drawerHeader.findViewById(R.id.userNameNav);
        userNameNav.setText(ParseUser.getCurrentUser().getUsername());
        TextView comNameNav = (TextView) drawerHeader.findViewById(R.id.comNameNav);
        comNameNav.setText(ParseUser.getCurrentUser().getString("ComunidadeLogada"));


        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


    }

    void showToast(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }




    private void setupViewPager(ViewPager viewPager) {
        adapter = new ViewPagerAdapter(getSupportFragmentManager());

        adapter.addFrag(new ComunidadeMainFragment(), nomeCom);

        viewPager.setAdapter(adapter);

    }





    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.comunidade_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.leave) {
            final ProgressDialog dlg = new ProgressDialog(MainActivity_Comunidade.this);
            dlg.setTitle("A sair da comunidade.");
            dlg.setMessage("Por favor, espere...");
            dlg.show();
            ParseQuery<ParseObject> query = new ParseQuery<ParseObject>("Comunidade");
            query.whereEqualTo("Nome", nomeCom);
            query.findInBackground(new FindCallback<ParseObject>() {
                @Override
                public void done(List<ParseObject> list, ParseException e) {
                    ParseObject comunidade = list.get(0);
                    comunidade.removeAll("Users", Arrays.asList(ParseUser.getCurrentUser().getUsername()));
                    comunidade.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            dlg.dismiss();
                            logoutFromCom(ParseUser.getCurrentUser().getUsername());
                            Intent intent = new Intent(MainActivity_Comunidade.this, DispatchActivity.class);
                            if(intent != null) {
                                startActivity(intent);
                            }
                        }
                    });
                }
            });

        }

        return super.onOptionsItemSelected(item);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_tarefas) {
            Intent intent = new Intent(this, MainActivity_Tarefas.class);
            if(intent != null) {
                startActivity(intent);
            }
        } else if (id == R.id.nav_despesas) {
            Intent intent = new Intent(this, MainActivity_Despesas.class);
            if(intent != null) {
                startActivity(intent);
            }
        } else if (id == R.id.nav_comunidade) {
            Intent intent = new Intent(this, MainActivity_Comunidade.class);
            if(intent != null) {
                startActivity(intent);
            }
        } else if (id == R.id.nav_aboutus) {
            Intent intent = new Intent(this, AboutUsActivity.class);
            if(intent != null) {
                startActivity(intent);
            }

        } else if (id == R.id.ic_menu_logout_com) {
            final ProgressDialog dlg = new ProgressDialog(MainActivity_Comunidade.this);
            dlg.setTitle("A fazer logout.");
            dlg.setMessage("Por favor, espere...");
            dlg.show();
            logoutFromCom(ParseUser.getCurrentUser().getUsername());
            dlg.dismiss();
            Intent intent = new Intent(this, DispatchActivity.class);
            if(intent != null) {
                startActivity(intent);
            }
        } else if (id == R.id.ic_menu_logout_MrRoomie) {
            final ProgressDialog dlg = new ProgressDialog(MainActivity_Comunidade.this);
            dlg.setTitle("A fazer logout.");
            dlg.setMessage("Por favor, espere...");
            dlg.show();
            logoutFromCom(ParseUser.getCurrentUser().getUsername());
            ParseUser.logOut();
            dlg.dismiss();
            Intent intent = new Intent(this, DispatchActivity.class);
            startActivity(intent);
        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void logoutFromCom(String user) {
        ParseUser cur = ParseUser.getCurrentUser();
        cur.put("ComunidadeLogada", "none");
        cur.saveInBackground();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        logoutFromCom(ParseUser.getCurrentUser().getUsername());
    }
}
