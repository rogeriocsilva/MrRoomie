package com.example.franciscomagalhaes.mrrom.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.franciscomagalhaes.mrrom.R;
import com.example.franciscomagalhaes.mrrom.adapters.SimpleRecyclerAdapter;
import com.example.franciscomagalhaes.mrrom.classes.UsersCom;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;


public class ComunidadeMainFragment extends Fragment {
    int color;
    UserCardViewAdapter adapter;
    List<UsersCom> users;
    Boolean tempBool;
    String tempPass;
    RecyclerView recyclerView;

    public ComunidadeMainFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.comunidade_main_fragment, container, false);

        final FrameLayout frameLayout = (FrameLayout) view.findViewById(R.id.com_bg);
        frameLayout.setBackgroundColor(color);

        recyclerView = (RecyclerView) view.findViewById(R.id.users_scrollableview);



        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity().getBaseContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);


        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.addUser);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View v2 = (LayoutInflater.from(getActivity())).inflate(R.layout.user_input, null);

                final AlertDialog.Builder alertBuilder = new AlertDialog.Builder(getActivity());
                alertBuilder.setTitle("Adicionar um utilizador!");
                alertBuilder.setView(v2);
                final EditText password = (EditText) v2.findViewById(R.id.userinputQuantia);
                final EditText user = (EditText) v2.findViewById(R.id.userinputDesc);
                password.setHint("Palavra-Passe da comunidade");
                user.setHint("Utilizador");

                alertBuilder.setPositiveButton("Adicionar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ParseQuery<ParseUser> queryUser = ParseQuery.getUserQuery();
                        queryUser.whereEqualTo("username", user.getText().toString());
                        queryUser.findInBackground(new FindCallback<ParseUser>() {
                            @Override
                            public void done(List<ParseUser> list, ParseException e) {
                                if (list.size() == 1) {
                                    ParseQuery<ParseObject> query = ParseQuery.getQuery("Comunidade");
                                    query.whereEqualTo("Nome", ParseUser.getCurrentUser().getString("ComunidadeLogada"));
                                    query.findInBackground(new FindCallback<ParseObject>() {
                                        @Override
                                        public void done(List<ParseObject> list, ParseException e) {
                                            ParseObject comunidade = list.get(0);
                                            if (password.getText().toString().equals(comunidade.getString("Password"))) {
                                                if (!comunidade.getList("Users").contains(user.getText().toString())) {
                                                    addToRecycler(new UsersCom(user.getText().toString(), false));
                                                    comunidade.add("Users", user.getText().toString());
                                                    comunidade.saveInBackground(new SaveCallback() {
                                                        @Override
                                                        public void done(ParseException e) {
                                                            Toast.makeText(getActivity(), "User adicionado com sucesso!", Toast.LENGTH_SHORT).show();

                                                        }
                                                    });
                                                } else {
                                                    Toast.makeText(getActivity(), "User já se encontra na comunidade!", Toast.LENGTH_SHORT).show();

                                                }

                                            } else {
                                                Toast.makeText(getActivity(), "Palavra-Passe Incorrecta!", Toast.LENGTH_SHORT).show();

                                            }
                                        }
                                    });
                                } else {
                                    Toast.makeText(getContext(), "User não existente!", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                    }
                });

                Dialog dialog = alertBuilder.create();
                dialog.show();

            }
        });

        populateRecycler();

        return view;
    }

    public void addToRecycler(UsersCom user) {

        if (users.size() == 0) {
            adapter = new UserCardViewAdapter(users);
            recyclerView.setAdapter(adapter);
        }

        users.add(user);

        adapter.notifyItemInserted(users.size() - 1);
    }


    public void populateRecycler() {

        String com = ParseUser.getCurrentUser().getString("ComunidadeLogada");

        ParseQuery<ParseObject> query = ParseQuery.getQuery("Comunidade");
        query.whereEqualTo("Nome", com);
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, com.parse.ParseException e) {
                List usersComunidade = list.get(0).getList("Users");

                Log.d("info", usersComunidade.toString());
                users = new ArrayList<UsersCom>();

                getUsersLogados(usersComunidade);


            }
        });

    }

    public void getUsersLogados(final List usersComunidade) {
        ParseQuery<ParseUser> query = ParseUser.getQuery();
        query.whereEqualTo("ComunidadeLogada", ParseUser.getCurrentUser().getString("ComunidadeLogada"));
        query.findInBackground(new FindCallback<ParseUser>() {
            @Override
            public void done(List<ParseUser> list, ParseException e) {
                ArrayList<UsersCom> usersLogados = new ArrayList<UsersCom>();
                ArrayList<UsersCom> usersNaoLogados = new ArrayList<UsersCom>();
                ArrayList<String> logados = new ArrayList<String>();
                for (int k = 0; k < list.size(); k++) {
                    usersLogados.add(new UsersCom(list.get(k).getString("username"), true));
                    logados.add(list.get(k).getString("username"));
                }

                for (int i = 0; i < usersComunidade.size(); i++) {
                    if (!logados.contains(usersComunidade.get(i).toString()))
                        usersNaoLogados.add(new UsersCom(usersComunidade.get(i).toString(), false));
                }


                users.addAll(usersLogados);
                users.addAll(usersNaoLogados);

                adapter=new UserCardViewAdapter(users);

                recyclerView.setAdapter(adapter);
        }
    });


    }


    /**
     * A simple adapter that loads a CardView layout with one TextView and two Buttons, and
     * listens to clicks on the Buttons or on the CardView
     */
    public class UserCardViewAdapter extends RecyclerView.Adapter<UserCardViewAdapter.ViewHolder> {
        private List<UsersCom> mUsers;

        public UserCardViewAdapter(List<UsersCom> mUsers) {
            this.mUsers = mUsers;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.comunidade_users_recycler_view_card_item, viewGroup, false);
            return new ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(ViewHolder viewHolder, int i) {
            UsersCom user = mUsers.get(i);
            viewHolder.user.setText(user.getUsername());

        }

        @Override
        public int getItemCount() {
            return mUsers == null ? 0 : mUsers.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            private TextView user;

            public ViewHolder(View itemView) {
                super(itemView);
                user = (TextView) itemView.findViewById(R.id.user);
            }
        }
    }
}