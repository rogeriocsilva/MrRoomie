package com.example.franciscomagalhaes.mrrom.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.franciscomagalhaes.mrrom.R;
import com.example.franciscomagalhaes.mrrom.classes.Tarefa;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.example.franciscomagalhaes.mrrom.classes.Comunidade;

public class LoginComunidadeActivity extends AppCompatActivity implements View.OnClickListener {

    private Toolbar toolbar;
    private RecyclerView recycler;
    private Button btnLogin;
    private TextView btnNewCom;
    private ArrayList<String> listaRecycler;
    private RecyclerComunidadeAdapter adapter;
    OnItemTouchListener itemTouchListener;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_comunidade);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ParseUser.logOut();
                Intent intent = new Intent(LoginComunidadeActivity.this, LogInActivity.class);
                startActivity(intent);
            }
        });

        recycler = (RecyclerView) findViewById(R.id.recyclerView);
        listaRecycler = new ArrayList<>();

        btnLogin = (Button) findViewById(R.id.btnLogin);
        btnNewCom = (TextView) findViewById(R.id.btnNewCom);

        btnLogin.setOnClickListener(this);
        btnNewCom.setOnClickListener(this);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this.getBaseContext());
        recycler.setLayoutManager(linearLayoutManager);
        recycler.setHasFixedSize(true);

        populateRecycler();

        itemTouchListener = new OnItemTouchListener() {

            @Override
            public void onComClicked(View view, int position) {
                if(!listaRecycler.get(position).equals("Nao tem comunidades ainda!"))
                    tryLogin(listaRecycler.get(position));
            }
        };
    }

    public void populateRecycler() {
        ParseQuery<ParseObject> query = new ParseQuery("Comunidade");
        query.whereEqualTo("Users", ParseUser.getCurrentUser().getUsername());
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                if (e == null) {
                    for (ParseObject obj : list) {
                        listaRecycler.add(obj.getString("Nome"));
                    }

                    if (listaRecycler.size() == 0) {
                        listaRecycler.add("Nao tem comunidades ainda!");
                    }

                    adapter = new RecyclerComunidadeAdapter(listaRecycler, itemTouchListener);
                    recycler.setAdapter(adapter);

                } else {
                    Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnLogin:
                startActivity(new Intent(LoginComunidadeActivity.this, LoginNovaComunidadeActivity.class));
                break;
            case R.id.btnNewCom:
                startActivity(new Intent(LoginComunidadeActivity.this, CreateComunidadeActivity.class));
                break;
        }
    }



    private void tryLogin(String nomeCom) {
        // Set up a progress dialog
        final ProgressDialog dlg = new ProgressDialog(LoginComunidadeActivity.this);
        dlg.setTitle("A entrar.");
        dlg.setMessage("Por favor, espere...");
        dlg.show();

        // Verifica se comunidade existe
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Comunidade");
        query.whereEqualTo("Nome", nomeCom);
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                if (list.size() != 0) {
                    ParseObject object = list.get(0);

                    ParseUser curUser = ParseUser.getCurrentUser();
                    curUser.put("ComunidadeLogada", list.get(0).getString("Nome"));
                    curUser.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            dlg.dismiss();
                            startActivity(new Intent(LoginComunidadeActivity.this, MainActivity_Comunidade.class));
                        }
                    });
                } else {
                    Toast.makeText(LoginComunidadeActivity.this, "Credencias inválidas!", Toast.LENGTH_LONG).show();
                    dlg.dismiss();
                }
            }
        });
    }



    /**
     * Interface for the touch events in each item
     */
    public interface OnItemTouchListener {

        public void onComClicked(View view, int position);


    }

    /**
     * A simple adapter that loads a CardView layout with one TextView, and
     * listens to clicks on the Buttons or on the CardView
     */
    public class RecyclerComunidadeAdapter extends RecyclerView.Adapter<RecyclerComunidadeAdapter.ViewHolder> {
        private List<String> mComunidades;
        private OnItemTouchListener onItemTouchListener;

        public RecyclerComunidadeAdapter(List<String> mComunidades, OnItemTouchListener onItemTouchListener) {
            this.mComunidades = mComunidades;
            this.onItemTouchListener = onItemTouchListener;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.comunidade_card_view, viewGroup, false);
            return new ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(ViewHolder viewHolder, int i) {
            String comunidade = mComunidades.get(i);
            viewHolder.nomeCom.setText(comunidade);
        }

        @Override
        public int getItemCount() {
            return mComunidades == null ? 0 : mComunidades.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            private TextView nomeCom;

            public ViewHolder(View itemView) {
                super(itemView);
                nomeCom = (TextView) itemView.findViewById(R.id.nomeCom);

                nomeCom.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onItemTouchListener.onComClicked(v, getPosition());

                    }
                });
            }
        }
    }
}
