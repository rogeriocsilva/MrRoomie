package com.example.franciscomagalhaes.mrrom.fragments;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.franciscomagalhaes.mrrom.R;
import com.example.franciscomagalhaes.mrrom.adapters.ListViewAdapter;
import com.example.franciscomagalhaes.mrrom.classes.Despesa;
import com.parse.DeleteCallback;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;


import org.w3c.dom.Text;

import java.lang.reflect.Array;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;


public class DespesaComunidadeFragment extends Fragment {
    CardViewAdapter adapter;
    ArrayList<Despesa> listaRecycler;
    RecyclerView recyclerView;
    String nomeCom;
    ArrayList<String> listaUsers;
    ArrayList<String> listaUsersChecked;
    String autorTemp;
    Integer positionTemp;
    ArrayList<String> listaUsersAux;
    OnItemTouchListener itemTouchListener;

    public DespesaComunidadeFragment() {}


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.despesa_pessoal_fragment, container, false);

        getUsersComunidade(ParseUser.getCurrentUser().getString("ComunidadeLogada"));


        nomeCom = ParseUser.getCurrentUser().getString("ComunidadeLogada");

        listaRecycler = new ArrayList<Despesa>();

        final FrameLayout frameLayout = (FrameLayout) view.findViewById(R.id.dummyfrag_bg);

        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity().getBaseContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);

        populateRecycler();

        itemTouchListener = new OnItemTouchListener() {
            @Override
            public void onCardViewTap(View view, int position) {
                ParseQuery<ParseObject> query = new ParseQuery<ParseObject>("Despesa");
                autorTemp = listaRecycler.get(position).getAutor();
                positionTemp = position;
                query.getInBackground(listaRecycler.get(position).getID(), new GetCallback<ParseObject>() {
                    @Override
                    public void done(final ParseObject parseObject, ParseException e) {
                        List users = parseObject.getList("Users");
                        String pagamento = parseObject.getString("PagamentPorUser");
                        listaUsersAux = new ArrayList<>();
                        ArrayList<String> listaPagamento = new ArrayList<String>();

                        for (int i = 0; i < users.size(); i++) {
                            listaUsersAux.add(users.get(i).toString());
                            listaPagamento.add(pagamento+"€");
                        }

                        if (listaUsersAux.size() == 0) {
                            AlertDialog.Builder builderVazio = new AlertDialog.Builder(getContext());
                            builderVazio.setTitle("Despesa paga!");
                            builderVazio.setPositiveButton("Ok!", null);
                            AlertDialog dialog2 = builderVazio.create();
                            dialog2.show();

                        } else {
                            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

                            listaUsers = new ArrayList<String>();

                            ListViewAdapter lviewAdapter = new ListViewAdapter(getActivity(), listaUsersAux.toArray(new String[listaUsersAux.size()]), listaPagamento.toArray(new String[listaPagamento.size()]));

                            ListView modeList = new ListView(getContext());
                            modeList.setAdapter(lviewAdapter);


                            if (ParseUser.getCurrentUser().getUsername().equals(autorTemp)) {
                                builder.setTitle("Selecionar Utilizadores;");
                                modeList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                    @Override
                                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                        if(listaUsers.contains(listaUsersAux.get(position))) {
                                            listaUsers.remove(listaUsersAux.get(position));
                                            TextView textEdit = (TextView) view.findViewById(R.id.textView1);
                                            textEdit.setText(textEdit.getText().toString().replace("Pago - ", ""));
                                        } else {
                                            listaUsers.add(listaUsersAux.get(position));
                                            TextView textEdit = (TextView) view.findViewById(R.id.textView1);
                                            textEdit.setText("Pago - " + textEdit.getText());
                                        }

                                    }
                                });


                                builder.setPositiveButton("Actualizar!", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        ParseQuery<ParseObject> query = new ParseQuery<ParseObject>("Despesa");
                                        query.getInBackground(parseObject.getObjectId(), new GetCallback<ParseObject>() {
                                            @Override
                                            public void done(ParseObject parseObject, ParseException e) {
                                                if (listaUsers.size() == parseObject.getList("Users").size()) {
                                                    parseObject.put("Done", true);
                                                    listaRecycler.get(positionTemp).setDone(true);
                                                    adapter.notifyItemChanged(positionTemp);
                                                }
                                                parseObject.removeAll("Users", listaUsers);
                                                parseObject.saveInBackground();

                                            }
                                        });
                                    }
                                });

                            } else{
                                builder.setTitle("Devedores:");
                                builder.setPositiveButton("Ok!", null);
                            }


                            builder.setView(modeList);
                            AlertDialog dialog1 = builder.create();
                            dialog1.show();
                        }



                }
                });

            }

            @Override
            public void onButtonRemoveClick(View view, int position) {
                removeFromParse(listaRecycler.get(position));
                removePos(position);
            }
        };

        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.addDespesa);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                View v = (LayoutInflater.from(getActivity())).inflate(R.layout.user_input, null);

                final AlertDialog.Builder alertBuilder = new AlertDialog.Builder(getActivity());
                alertBuilder.setView(v);
                final EditText userInputDesc = (EditText) v.findViewById(R.id.userinputDesc);
                final EditText userInputQuantia = (EditText) v.findViewById(R.id.userinputQuantia);


                alertBuilder.setNegativeButton("Cancelar!", null);

                alertBuilder.setPositiveButton("Avançar!", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        if (checkNumberInput(userInputQuantia.getText().toString()) && userInputDesc.getText().length() > 0) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                            builder.setTitle("Selecione os contribuidores.");

                            listaUsers = new ArrayList<String>();


                            builder.setPositiveButton("Adicionar!", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    final Despesa novaDespesa = new Despesa();
                                    novaDespesa.put("Autor", ParseUser.getCurrentUser().getUsername());
                                    novaDespesa.put("Quantia", userInputQuantia.getText().toString());
                                    novaDespesa.put("Descricao", userInputDesc.getText().toString());
                                    novaDespesa.put("Comunidade", nomeCom);
                                    novaDespesa.put("Users", listaUsers);
                                    novaDespesa.put("Done", false);

                                    double quantia = Double.parseDouble(userInputQuantia.getText().toString());
                                    double users = listaUsers.size();
                                    final double preco = quantia/(users + 1.0);
                                    double precoAux = roundTwoDecimals(preco);

                                    novaDespesa.put("PagamentPorUser",String.valueOf(precoAux));

                                    novaDespesa.saveInBackground(new SaveCallback() {
                                        @Override
                                        public void done(ParseException e) {
                                            addToRecycler(new Despesa(ParseUser.getCurrentUser().getUsername(), userInputDesc.getText().toString(), userInputQuantia.getText().toString(), false, novaDespesa.getObjectId()));
                                        }
                                    });
                                }
                            });

                            ListView modeList = new ListView(getContext());
                            ArrayAdapter<String> modeAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, android.R.id.text1, listaUsersChecked.toArray(new String[listaUsersChecked.size()]));
                            modeList.setAdapter(modeAdapter);
                            modeList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                    if (((TextView) view).getText().toString().startsWith("Adicionado - ")) {
                                        ((TextView) view).setText(((TextView) view).getText().toString().replace("Adicionado - ", ""));
                                        listaUsers.remove(((TextView) view).getText());
                                    } else {
                                        listaUsers.add(((TextView) view).getText().toString());
                                        ((TextView) view).setText("Adicionado - " + ((TextView) view).getText());
                                    }


                                }
                            });


                            builder.setView(modeList);
                            AlertDialog dialog1 = builder.create();
                            dialog1.show();
                        } else {
                            Toast.makeText(getContext(), "Insira uma despesa válida!", Toast.LENGTH_SHORT).show();
                        }


                    }
                });

                Dialog dialog = alertBuilder.create();
                dialog.show();


            }
        });



        return view;
    }

    double roundTwoDecimals(double d) {
        DecimalFormat twoDForm = new DecimalFormat("#.##");
        return Double.valueOf(twoDForm.format(d));
    }

    public boolean checkNumberInput(String quantia) {
        try {
            double d = Double.parseDouble(quantia);
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }


    public void getUsersComunidade(String nomeCom) {
        ParseQuery<ParseObject> query = new ParseQuery<ParseObject>("Comunidade");
        query.whereEqualTo("Nome", nomeCom);
        listaUsersChecked = new ArrayList<>();
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                List users = list.get(0).getList("Users");
                for (int i = 0; i < users.size(); i++) {
                    if (!users.get(i).toString().equals(ParseUser.getCurrentUser().getUsername())) {
                        listaUsersChecked.add(users.get(i).toString());
                    }
                }
            }
        });
    }



    public void removeFromParse(Despesa desp) {
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Despesa");
        query.getInBackground(desp.getID(), new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject parseObject, ParseException e) {
                parseObject.deleteInBackground();
            }
        });
    }

    public void populateRecycler() {
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Despesa");
        query.whereEqualTo("Comunidade", nomeCom);
        query.addAscendingOrder("Done");
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                Log.d("Tamanho", list.size() + "");
                if (e == null) {
                    for (ParseObject obj : list) {
                        Despesa novaDesp = new Despesa(obj.getString("Autor"), obj.getString("Descricao"), obj.getString("Quantia"), obj.getBoolean("Done"), obj.getObjectId());
                        listaRecycler.add(novaDesp);
                    }

                    if (listaRecycler != null) {
                        adapter = new CardViewAdapter(listaRecycler, itemTouchListener);
                        recyclerView.setAdapter(adapter);
                    }

                } else {
                    Toast.makeText(getContext(), e.toString(), Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    public void addToRecycler(Despesa novaDesp) {

        if (listaRecycler.size() == 0) {
            adapter = new CardViewAdapter(listaRecycler, itemTouchListener);
            recyclerView.setAdapter(adapter);
        }

        listaRecycler.add(0, novaDesp);

        adapter.notifyItemInserted(0);
    }

    public void removePos(int position) {
        listaRecycler.remove(position);
        recyclerView.removeViewAt(position);
        adapter.notifyItemRemoved(position);
        adapter.notifyItemRangeChanged(position, listaRecycler.size());
    }

    /**
     * Interface for the touch events in each item
     */
    public interface OnItemTouchListener {
        /**
         * Callback invoked when the user Taps one of the RecyclerView items
         *
         * @param view     the CardView touched
         * @param position the index of the item touched in the RecyclerView
         */
        public void onCardViewTap(View view, int position);

        /**
         * Callback invoked when the ButtonRemove of an item is touched
         *
         * @param view     the Button touched
         * @param position the index of the item touched in the RecyclerView
         */
        public void onButtonRemoveClick(View view, int position);

    }

    /**
     * A simple adapter that loads a CardView layout with one TextView and two Buttons, and
     * listens to clicks on the Buttons or on the CardView
     */
    public class CardViewAdapter extends RecyclerView.Adapter<CardViewAdapter.ViewHolder> {
        private List<Despesa> mDespesas;
        private OnItemTouchListener onItemTouchListener;

        public CardViewAdapter(List<Despesa> mDespesas, OnItemTouchListener onItemTouchListener) {
            this.mDespesas = mDespesas;
            this.onItemTouchListener = onItemTouchListener;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.despesa_pessoal_recycler_view_card_item, viewGroup, false);
            return new ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(ViewHolder viewHolder, int i) {
            Despesa desp = mDespesas.get(i);
            viewHolder.quantia.setText(desp.getQuantidade() + "€");
            if (desp.getDone()) {
                viewHolder.desc.setText(desp.getDesc());
                viewHolder.desc.setPaintFlags(viewHolder.desc.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

            } else {
                if(!desp.getAutor().equals(ParseUser.getCurrentUser().getUsername())) {
                    viewHolder.removeItem.setVisibility(View.GONE);
                }
                viewHolder.desc.setPaintFlags(0);
                viewHolder.desc.setText(desp.getDesc());
            }
        }

        @Override
        public int getItemCount() {
            return mDespesas == null ? 0 : mDespesas.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            private TextView desc;
            private TextView quantia;
            private ImageView removeItem;

            public ViewHolder(View itemView) {
                super(itemView);
                desc = (TextView) itemView.findViewById(R.id.descricao);
                quantia = (TextView) itemView.findViewById(R.id.quantia);
                removeItem = (ImageView) itemView.findViewById(R.id.removeItem);

                removeItem.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onItemTouchListener.onButtonRemoveClick(v, getPosition());

                    }
                });

                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onItemTouchListener.onCardViewTap(v, getPosition());

                    }
                });
            }
        }
    }
}