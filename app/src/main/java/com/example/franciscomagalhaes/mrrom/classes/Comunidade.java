package com.example.franciscomagalhaes.mrrom.classes;

import com.parse.ParseClassName;
import com.parse.ParseObject;

import java.util.List;

/**
 * Created by franciscomagalhaes on 17/11/15.
 */

@ParseClassName("Comunidade")
public class Comunidade extends ParseObject {

    public Comunidade() {}

    public void setUsers(List users) {
        put("Users", users);
    }

    public void setPassword(String password) {
        put("Password", password);
    }

    public void setNome(String nome) {
        put("Nome", nome);
    }

    public List getUserLogged() {
        return getList("UsersLogados");
    }

    public List getUsernames() {
        return getList("Users");
    }

    public List getTarefas() {
        return getList("Tarefas");
    }

    public List getDespesas() {
        return getList("Despesas");
    }

    public int getQuantidade() {
        return getInt("Quantidade");
    }

    public void setDesc(String desc) {
        put("Desc", desc);
    }

    public String getDesc() {
        return getString("Desc");
    }

    public void setQuantidade(int quantidade) {
        put("Quantidade", quantidade);
    }
}
