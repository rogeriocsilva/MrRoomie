package com.example.franciscomagalhaes.mrrom.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.franciscomagalhaes.mrrom.R;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.parse.SignUpCallback;

public class LogInActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btnLogin;
    private TextView btnSignUp;
    private TextInputLayout usernameWrapper;
    private TextInputLayout passwordWrapper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        //toolbar = (Toolbar) findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);

        btnLogin = (Button) findViewById(R.id.btnLogin);
        btnSignUp = (TextView) findViewById(R.id.btnSignUp);

        usernameWrapper = (TextInputLayout) findViewById(R.id.usernameWrapper);
        passwordWrapper = (TextInputLayout) findViewById(R.id.passwordWrapper);

        usernameWrapper.setHint("Nome de Utilizador");
        passwordWrapper.setHint("Palavra-Chave");

        btnLogin.setOnClickListener(this);
        btnSignUp.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnLogin:
                if (checkData()) {
                    tryLogin();
                }
                break;
            case R.id.btnSignUp:
                Intent intent = new Intent(LogInActivity.this, SignUpActivity.class);
                startActivity(intent);
                break;
        }
    }


    private void tryLogin() {
        final ProgressDialog dlg = new ProgressDialog(LogInActivity.this);
        dlg.setTitle("A entrar.");
        dlg.setMessage("Por favor, espere...");
        dlg.show();
        // Call the Parse login method
        ParseUser.logInInBackground(usernameWrapper.getEditText().getText().toString(), passwordWrapper.getEditText().getText().toString(), new LogInCallback() {

            @Override
            public void done(ParseUser user, ParseException e) {
                dlg.dismiss();
                if (e != null) {
                    // Show the error message
                    Toast.makeText(LogInActivity.this,"Credênciais inválidas!", Toast.LENGTH_LONG).show();
                } else {
                    // Start an intent for the dispatch activity
                    Toast.makeText(LogInActivity.this, "Login efectuado!", Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(LogInActivity.this, LoginComunidadeActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }
            }
        });
    }

    private boolean checkData() {
        boolean validationError = false;
        StringBuilder validationErrorMessage = new StringBuilder(getResources().getString(R.string.error_intro));
        if (isEmpty(usernameWrapper.getEditText().getText().toString())) {
            validationError = true;
            validationErrorMessage.append(getResources().getString(R.string.error_blank_username));
        }
        else if (isEmpty(passwordWrapper.getEditText().getText().toString())) {
            validationError = true;
            validationErrorMessage.append(getResources().getString(R.string.error_blank_password));
        }
        validationErrorMessage.append(getResources().getString(R.string.error_end));

        // If there is a validation error, display the error
        if (validationError) {
            Toast.makeText(LogInActivity.this, validationErrorMessage.toString(), Toast.LENGTH_LONG).show();
            return false;
        }

        return true;
    }

    private boolean isEmpty(String text) {
        if (text.length() > 0) {
            return false;
        } else {
            return true;
        }
    }
}
