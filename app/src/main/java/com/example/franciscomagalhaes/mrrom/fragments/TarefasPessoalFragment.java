package com.example.franciscomagalhaes.mrrom.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.franciscomagalhaes.mrrom.R;
import com.example.franciscomagalhaes.mrrom.classes.Tarefa;
import com.google.android.gms.maps.model.TileOverlay;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;


import java.util.ArrayList;
import java.util.List;


public class TarefasPessoalFragment extends Fragment {
    CardViewAdapter adapter;
    ArrayList<Tarefa> listaRecycler;
    RecyclerView recyclerView;
    OnItemTouchListener itemTouchListener;

    public TarefasPessoalFragment() {}



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.tarefas_pessoal_fragment, container, false);

        listaRecycler = new ArrayList<Tarefa>();

        final FrameLayout frameLayout = (FrameLayout) view.findViewById(R.id.bg);

        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity().getBaseContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);

        populateRecycler();

        itemTouchListener = new OnItemTouchListener() {
            @Override
            public void onCardViewTap(View view, int position) {
                Toast.makeText(getActivity(), "Tapped " + listaRecycler.get(position).getDesc(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onButtonRemoveClick(View view, int position) {
                Toast.makeText(getActivity(), "Eliminar entrada -> " + listaRecycler.get(position).getDesc(), Toast.LENGTH_SHORT).show();
                removeFromParse(listaRecycler.get(position));
                removePos(position);
            }

            @Override
            public void onButtonDoneClick(View view, int position) {
                if (listaRecycler.get(position).getDone()) {
                    listaRecycler.get(position).setDone(false);
                    setDoneParse(listaRecycler.get(position), false);

                } else {
                    listaRecycler.get(position).setDone(true);
                    setDoneParse(listaRecycler.get(position), true);
                }
                adapter.notifyItemChanged(position);
            }

        };

        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.addTarefa);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                View v = (LayoutInflater.from(getActivity())).inflate(R.layout.user_input_tarefa_pessoal, null);

                AlertDialog.Builder alertBuilder = new AlertDialog.Builder(getActivity());
                alertBuilder.setView(v);
                final EditText userInputDesc = (EditText) v.findViewById(R.id.userinputDesc);

                alertBuilder.setCancelable(true)
                        .setPositiveButton("Adicionar!", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if (userInputDesc.getText().toString().length() > 0) {
                                    final Tarefa novaTarefa = new Tarefa();
                                    novaTarefa.put("Autor", ParseUser.getCurrentUser().getUsername());
                                    novaTarefa.put("Descricao", userInputDesc.getText().toString());
                                    novaTarefa.put("Done", false);
                                    novaTarefa.put("Comunidade", "none");
                                    novaTarefa.saveInBackground(new SaveCallback() {
                                        @Override
                                        public void done(ParseException e) {
                                            addToRecycler(new Tarefa(ParseUser.getCurrentUser().getUsername(), userInputDesc.getText().toString(), false, novaTarefa.getObjectId(), null));
                                            Toast.makeText(getContext(), "Despesa Salva!", Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                } else {
                                    Toast.makeText(getContext(), "Não inseriu Tarefa!", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });

                Dialog dialog = alertBuilder.create();
                dialog.show();


            }
        });

        return view;
    }

    public void setDoneParse(Tarefa tarefa, final Boolean done) {
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Tarefa");
        query.getInBackground(tarefa.getID(), new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject parseObject, ParseException e) {
                parseObject.put("Done", done);
                parseObject.saveInBackground();
            }
        });

    }


    public void removeFromParse(Tarefa tarefa) {
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Tarefa");
        query.getInBackground(tarefa.getID(), new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject parseObject, ParseException e) {
                parseObject.deleteInBackground();
            }
        });
    }

    public void populateRecycler() {
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Tarefa");
        query.whereEqualTo("Autor", ParseUser.getCurrentUser().getUsername());
        query.whereEqualTo("Comunidade", "none");
        query.addAscendingOrder("Done");
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                if (e == null) {
                    for (ParseObject obj : list) {
                        Tarefa novaTarefa = new Tarefa(obj.getString("Autor"), obj.getString("Descricao"), obj.getBoolean("Done"), obj.getObjectId(), null);
                        listaRecycler.add(novaTarefa);
                    }

                    if (listaRecycler.size() != 0) {
                        adapter = new CardViewAdapter(listaRecycler, itemTouchListener);
                        recyclerView.setAdapter(adapter);
                    }

                } else {
                    Toast.makeText(getContext(), e.toString(), Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    public void addToRecycler(Tarefa tarefa) {

        if (listaRecycler.size() == 0) {
            adapter = new CardViewAdapter(listaRecycler, itemTouchListener);
            recyclerView.setAdapter(adapter);
        }

        listaRecycler.add(0, tarefa);

        adapter.notifyItemInserted(0);
    }

    public void removePos(int position) {
        listaRecycler.remove(position);
        recyclerView.removeViewAt(position);
        adapter.notifyItemRemoved(position);
        adapter.notifyItemRangeChanged(position, listaRecycler.size());
    }

    /**
     * Interface for the touch events in each item
     */
    public interface OnItemTouchListener {
        /**
         * Callback invoked when the user Taps one of the RecyclerView items
         *
         * @param view     the CardView touched
         * @param position the index of the item touched in the RecyclerView
         */
        public void onCardViewTap(View view, int position);

        /**
         * Callback invoked when the ButtonRemove of an item is touched
         *
         * @param view     the Button touched
         * @param position the index of the item touched in the RecyclerView
         */


        public void onButtonRemoveClick(View view, int position);

        public void onButtonDoneClick(View view, int position);

    }

    /**
     * A simple adapter that loads a CardView layout with one TextView and two Buttons, and
     * listens to clicks on the Buttons or on the CardView
     */
    public class CardViewAdapter extends RecyclerView.Adapter<CardViewAdapter.ViewHolder> {
        private List<Tarefa> mTarefas;
        private OnItemTouchListener onItemTouchListener;

        public CardViewAdapter(List<Tarefa> mTarefas, OnItemTouchListener onItemTouchListener) {
            this.mTarefas = mTarefas;
            this.onItemTouchListener = onItemTouchListener;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.tarefa_pessoal_recycler_view_card_item, viewGroup, false);
            return new ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(ViewHolder viewHolder, int i) {
            Tarefa tar = mTarefas.get(i);
            viewHolder.desc.setText(tar.getDesc());

            Log.d("fuckingdebug", tar.toString() + tar.getDesc());

            if (tar.getDone()) {
                viewHolder.done.setText("Marcar Não-Concluído!");
                viewHolder.desc.setPaintFlags(viewHolder.desc.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            } else {
                viewHolder.desc.setPaintFlags(0);
                viewHolder.done.setText("Marcar Concluído!");
            }
        }

        @Override
        public int getItemCount() {
            return mTarefas == null ? 0 : mTarefas.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            private TextView desc;
            private Button done;
            private ImageView removeItem;

            public ViewHolder(View itemView) {
                super(itemView);
                desc = (TextView) itemView.findViewById(R.id.descricao);
                done = (Button) itemView.findViewById(R.id.done);
                removeItem = (ImageView) itemView.findViewById(R.id.removeItem);

                removeItem.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onItemTouchListener.onButtonRemoveClick(v, getPosition());

                    }
                });


                done.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onItemTouchListener.onButtonDoneClick(v, getPosition());

                    }
                });

                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onItemTouchListener.onCardViewTap(v, getPosition());

                    }
                });
            }
        }
    }
}