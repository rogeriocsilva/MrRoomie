package com.example.franciscomagalhaes.mrrom.fragments;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.franciscomagalhaes.mrrom.R;
import com.example.franciscomagalhaes.mrrom.classes.Despesa;
import com.parse.DeleteCallback;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;


import java.util.ArrayList;
import java.util.List;


public class DespesaPessoalFragment extends Fragment {
    int color;
    CardViewAdapter adapter;
    ArrayList<Despesa> listaRecycler;
    RecyclerView recyclerView;
    OnItemTouchListener itemTouchListener;

    public DespesaPessoalFragment() {}

    @SuppressLint("ValidFragment")
    public DespesaPessoalFragment(int color) {
        this.color = color;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.despesa_pessoal_fragment, container, false);

        listaRecycler = new ArrayList<Despesa>();

        final FrameLayout frameLayout = (FrameLayout) view.findViewById(R.id.dummyfrag_bg);
        frameLayout.setBackgroundColor(color);

        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity().getBaseContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);

        populateRecycler();

        itemTouchListener = new OnItemTouchListener() {
            @Override
            public void onCardViewTap(View view, int position) {
                Toast.makeText(getActivity(), "Tapped " + listaRecycler.get(position).getID(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onButtonRemoveClick(View view, int position) {
                removeFromParse(listaRecycler.get(position).getID());
                removePos(position);
            }
        };

        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.addDespesa);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                View v = (LayoutInflater.from(getActivity())).inflate(R.layout.user_input, null);

                AlertDialog.Builder alertBuilder = new AlertDialog.Builder(getActivity());
                alertBuilder.setView(v);
                final EditText userInputDesc = (EditText) v.findViewById(R.id.userinputDesc);
                final EditText userInputQuantia = (EditText) v.findViewById(R.id.userinputQuantia);


                alertBuilder.setCancelable(true)
                        .setPositiveButton("Adicionar!", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if(userInputDesc.getText().length() > 0 && quantiaIsOk(userInputQuantia.getText().toString())) {
                                    final Despesa novaDespesa = new Despesa();
                                    novaDespesa.put("Autor", ParseUser.getCurrentUser().getUsername());
                                    novaDespesa.put("Quantia", userInputQuantia.getText().toString());
                                    novaDespesa.put("Descricao", userInputDesc.getText().toString());
                                    novaDespesa.put("Comunidade", "none");
                                    novaDespesa.saveInBackground(new SaveCallback() {
                                        @Override
                                        public void done(ParseException e) {
                                            addToRecycler(new Despesa(ParseUser.getCurrentUser().getUsername(), userInputDesc.getText().toString(), userInputQuantia.getText().toString(), false, novaDespesa.getObjectId()));
                                            Toast.makeText(getContext(), "Salvo no Parse", Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                } else {
                                    Toast.makeText(getContext(), "Insira uma despesa válida!", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });

                Dialog dialog = alertBuilder.create();
                dialog.show();


            }
        });



        return view;
    }

    public boolean quantiaIsOk(String str) {
        try {
            double d = Double.parseDouble(str);
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }

    public void removeFromParse(String objectID) {
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Despesa");
        query.getInBackground(objectID, new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject parseObject, ParseException e) {
                parseObject.deleteInBackground();
            }
        });
    }

    public void populateRecycler() {
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Despesa");
        query.whereEqualTo("Autor", ParseUser.getCurrentUser().getUsername());
        query.whereEqualTo("Comunidade", "none");
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                if (e == null) {
                    for (ParseObject obj : list) {
                        Despesa novaDesp = new Despesa(obj.getString("Autor"), obj.getString("Descricao"), obj.getString("Quantia"), obj.getBoolean("Done"), obj.getObjectId());
                        listaRecycler.add(novaDesp);
                    }

                    if (listaRecycler != null) {
                        adapter = new CardViewAdapter(listaRecycler, itemTouchListener);
                        recyclerView.setAdapter(adapter);
                    }

                } else {
                    Toast.makeText(getContext(), e.toString(), Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    public void addToRecycler(Despesa novaDesp) {

        if (listaRecycler.size() == 0) {
            adapter = new CardViewAdapter(listaRecycler, itemTouchListener);
            recyclerView.setAdapter(adapter);
        }

        listaRecycler.add(0, novaDesp);

        adapter.notifyItemInserted(0);
    }

    public void removePos(int position) {
        listaRecycler.remove(position);
        recyclerView.removeViewAt(position);
        adapter.notifyItemRemoved(position);
        adapter.notifyItemRangeChanged(position, listaRecycler.size());
    }

    /**
     * Interface for the touch events in each item
     */
    public interface OnItemTouchListener {
        /**
         * Callback invoked when the user Taps one of the RecyclerView items
         *
         * @param view     the CardView touched
         * @param position the index of the item touched in the RecyclerView
         */
        public void onCardViewTap(View view, int position);

        /**
         * Callback invoked when the ButtonRemove of an item is touched
         *
         * @param view     the Button touched
         * @param position the index of the item touched in the RecyclerView
         */
        public void onButtonRemoveClick(View view, int position);

    }

    /**
     * A simple adapter that loads a CardView layout with one TextView and two Buttons, and
     * listens to clicks on the Buttons or on the CardView
     */
    public class CardViewAdapter extends RecyclerView.Adapter<CardViewAdapter.ViewHolder> {
        private List<Despesa> mDespesas;
        private OnItemTouchListener onItemTouchListener;

        public CardViewAdapter(List<Despesa> mDespesas, OnItemTouchListener onItemTouchListener) {
            this.mDespesas = mDespesas;
            this.onItemTouchListener = onItemTouchListener;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.despesa_pessoal_recycler_view_card_item, viewGroup, false);
            return new ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(ViewHolder viewHolder, int i) {
            Despesa desp = mDespesas.get(i);
            viewHolder.quantia.setText(desp.getQuantidade() + "€");
            viewHolder.desc.setText(desp.getDesc());

        }

        @Override
        public int getItemCount() {
            return mDespesas == null ? 0 : mDespesas.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            private TextView desc;
            private TextView quantia;
            private ImageView removeItem;

            public ViewHolder(View itemView) {
                super(itemView);
                desc = (TextView) itemView.findViewById(R.id.descricao);
                quantia = (TextView) itemView.findViewById(R.id.quantia);
                removeItem = (ImageView) itemView.findViewById(R.id.removeItem);

                removeItem.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onItemTouchListener.onButtonRemoveClick(v, getPosition());

                    }
                });

                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onItemTouchListener.onCardViewTap(v, getPosition());

                    }
                });
            }
        }
    }
}