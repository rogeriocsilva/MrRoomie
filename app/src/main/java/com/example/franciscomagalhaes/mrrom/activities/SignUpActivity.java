package com.example.franciscomagalhaes.mrrom.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.nfc.Tag;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.franciscomagalhaes.mrrom.R;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.parse.SignUpCallback;

public class SignUpActivity extends AppCompatActivity implements View.OnClickListener{

    private TextInputLayout userName, userPassword, userConfirmPassword;
    private Button btnSignUp;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sign_up_activity);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        btnSignUp = (Button) findViewById(R.id.createComunidade);

        userName = (TextInputLayout) findViewById(R.id.nameUserWrapper);
        userPassword = (TextInputLayout) findViewById(R.id.passwordWrapper);
        userConfirmPassword = (TextInputLayout) findViewById(R.id.confirm_passwordWrapper);

        btnSignUp.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        if(checkData()){
            createNewUser();
        }
    }


    private boolean checkData() {
        boolean validationError = false;
        StringBuilder validationErrorMessage =
                new StringBuilder(getResources().getString(R.string.error_intro));
        if (isEmpty(userName.getEditText().getText().toString())) {
            validationError = true;
            validationErrorMessage.append(getResources().getString(R.string.error_blank_user_name));
        } else if (isEmpty(userPassword.getEditText().getText().toString())) {
            validationError = true;
            validationErrorMessage.append(getResources().getString(R.string.error_blank_user_password));
        } else if (isEmpty(userConfirmPassword.getEditText().getText().toString())) {
            validationError = true;
            validationErrorMessage.append(getResources().getString(R.string.error_blank_user_confirm_password));
        } else if (!check_confirm_password()){
            validationError = true;
            validationErrorMessage.append(getResources().getString(R.string.error_mismatched_passwords));
        }
        validationErrorMessage.append(getResources().getString(R.string.error_end));
        // If there is a validation error, display the error
        if (validationError) {
            Toast.makeText(SignUpActivity.this, validationErrorMessage.toString(), Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    private void createNewUser() {
        // Set up a progress dialog
        final ProgressDialog dlg = new ProgressDialog(SignUpActivity.this);
        dlg.setTitle("A Registar.");
        dlg.setMessage("Por favor, espere...");
        dlg.show();

        // Set up a new Parse user
        ParseUser user = new ParseUser();
        user.put("TarefasDone", 0);
        user.setUsername(userName.getEditText().getText().toString());
        user.setPassword(userPassword.getEditText().getText().toString());
        // Call the Parse signup method
        user.signUpInBackground(new SignUpCallback() {
            @Override
            public void done(ParseException e) {
                dlg.dismiss();
                if (e != null) {
                    // Show the error message
                    Toast.makeText(SignUpActivity.this, "Erro: Nome de utilizador já se encontra registado", Toast.LENGTH_LONG).show();
                } else {
                    ParseUser userp = ParseUser.getCurrentUser();
                    userp.put("ComunidadeLogada", "none");
                    userp.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            Intent intent = new Intent(SignUpActivity.this, DispatchActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                            Toast.makeText(SignUpActivity.this, "Conta criada com sucesso!", Toast.LENGTH_LONG).show();
                            startActivity(intent);
                        }
                    });

                }
            }
        });

    }


    private boolean isEmpty(String text) {
        if (text.length() > 0) {
            return false;
        } else {
            return true;
        }
    }

    private boolean check_confirm_password() {
        return (userPassword.getEditText().getText().toString().equals(userConfirmPassword.getEditText().getText().toString()));
    }

}