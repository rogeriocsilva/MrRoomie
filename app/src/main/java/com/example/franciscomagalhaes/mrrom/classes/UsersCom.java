package com.example.franciscomagalhaes.mrrom.classes;

/**
 * Created by franciscomagalhaes on 23/12/15.
 */
public class UsersCom {

    Boolean logged;
    String username;

    public UsersCom(String username, Boolean logged) {
        this.logged = logged;
        this.username = username;
    }

    public Boolean getLogged() {
        return logged;
    }

    public void setLogged(Boolean logged) {
        this.logged = logged;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
