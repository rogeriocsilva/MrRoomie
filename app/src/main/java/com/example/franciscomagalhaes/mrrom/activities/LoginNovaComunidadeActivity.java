package com.example.franciscomagalhaes.mrrom.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.franciscomagalhaes.mrrom.R;
import com.parse.FindCallback;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.parse.SignUpCallback;

import java.util.List;

public class LoginNovaComunidadeActivity extends AppCompatActivity implements View.OnClickListener {

    private Toolbar toolbar;
    private Button btnLogin;
    private TextInputLayout comWrapper;
    private TextInputLayout passwordWrapper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_nova_comunidade);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        btnLogin = (Button) findViewById(R.id.btnLogin);

        comWrapper = (TextInputLayout) findViewById(R.id.comWrapper);
        passwordWrapper = (TextInputLayout) findViewById(R.id.passwordWrapper);

        comWrapper.setHint("Nome da Comunidade");
        passwordWrapper.setHint("Palavra-Chave");

        btnLogin.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnLogin:
                tryLogin();
                break;
        }
    }


    private void tryLogin() {
        // Set up a progress dialog
        final ProgressDialog dlg = new ProgressDialog(LoginNovaComunidadeActivity.this);
        dlg.setTitle("Please wait.");
        dlg.setMessage("Logging in.  Please wait.");
        dlg.show();

        // Verifica se comunidade existe
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Comunidade");
        query.whereEqualTo("Nome", comWrapper.getEditText().getText().toString());
        query.whereEqualTo("Password", passwordWrapper.getEditText().getText().toString());
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                if (list.size() != 0) {
                    ParseObject object = list.get(0);
                    if (!object.getList("Users").contains(ParseUser.getCurrentUser().getUsername())) {
                        object.add("Users", ParseUser.getCurrentUser().getUsername());
                        object.saveInBackground();
                    }

                    ParseUser curUser = ParseUser.getCurrentUser();
                    curUser.put("ComunidadeLogada", comWrapper.getEditText().getText().toString());
                    curUser.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            dlg.dismiss();
                            Toast.makeText(LoginNovaComunidadeActivity.this, "Login da Comunidade Concluido", Toast.LENGTH_LONG).show();
                            startActivity(new Intent(LoginNovaComunidadeActivity.this, MainActivity_Comunidade.class));
                        }
                    });
                } else {
                    Toast.makeText(LoginNovaComunidadeActivity.this, "Não existe nenhuma comunidade com essas credenciais", Toast.LENGTH_LONG).show();
                    dlg.dismiss();
                }
            }
        });
    }
}
