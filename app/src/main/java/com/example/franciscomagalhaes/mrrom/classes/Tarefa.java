package com.example.franciscomagalhaes.mrrom.classes;

import com.parse.ParseClassName;
import com.parse.ParseObject;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by franciscomagalhaes on 13/11/15.
 */

@ParseClassName("Tarefa")
public class Tarefa extends ParseObject {
    String autor;
    String desc;
    String objectID;
    Boolean done;
    ArrayList<String> users;

    public Tarefa(String autor, String desc, Boolean done, String objectID, ArrayList<String> users) {
        this.autor = autor;
        this.desc = desc;
        this.done = done;
        this.objectID = objectID;
        this.users = users;
    }

    public Tarefa() { }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public void setUsers(ArrayList<String> lista) {
        users = lista;
    }


    public void setDesc(String desc) {
        this.desc = desc;
    }

    public ArrayList<String> getUsers() {
        return this.users;
    }


    public String getAutor() {
        return autor;
    }

    public String getDesc() {
        return desc;
    }


    public Boolean getDone() {
        return this.done;
    }

    public void setDone(Boolean bool) {
        this.done = bool;
    }

    public String getID() {
        return this.objectID;
    }
}
