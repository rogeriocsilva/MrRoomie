package com.example.franciscomagalhaes.mrrom.classes;

import com.parse.ParseClassName;
import com.parse.ParseObject;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by franciscomagalhaes on 13/11/15.
 */

@ParseClassName("Despesa")
public class Despesa extends ParseObject {
    String autor;
    String desc;
    String quantia;
    String objectID;
    Boolean done;


    public List getUsers() {
        List<ParseObject> objects = getList("Users");
        return objects;
    }

    public Despesa(String autor, String desc, String quantia, Boolean done, String objectID) {
        this.autor = autor;
        this.done = done;
        this.desc = desc;
        this.quantia = quantia;
        this.objectID = objectID;
    }

    public Despesa() { }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getID() {
        return this.objectID;
    }

    public Boolean getDone() {
        return done;
    }

    public void setDone(Boolean done) {
        this.done = done;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public void setQuantidade(String quantidade) {
        this.quantia = quantidade;
    }

    public String getAutor() {
        return autor;
    }

    public String getDesc() {
        return desc;
    }

    public String getQuantidade() {
        return quantia;
    }
}
