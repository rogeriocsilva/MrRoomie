package com.example.franciscomagalhaes.mrrom.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.franciscomagalhaes.mrrom.R;
import com.example.franciscomagalhaes.mrrom.classes.Comunidade;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.util.List;

public class CreateComunidadeActivity extends AppCompatActivity implements View.OnClickListener {

    private TextInputLayout comunidadeName, comunidadePassword, comunidadeConfirmPassword;
    private Button btnCreate;
    private Toolbar toolbar;
    private boolean tempBool;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_comunidade_activity);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               onBackPressed();
            }
        });

        btnCreate = (Button) findViewById(R.id.createComunidade);

        comunidadeName = (TextInputLayout) findViewById(R.id.nameComunidadeWrapper);
        comunidadePassword = (TextInputLayout) findViewById(R.id.passwordWrapper);
        comunidadeConfirmPassword = (TextInputLayout) findViewById(R.id.confirm_passwordWrapper);

        btnCreate.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        if (checkData()) {
            ParseQuery<ParseObject> query = new ParseQuery<ParseObject>("Comunidade");
            query.whereEqualTo("Nome", comunidadeName.getEditText().getText().toString());
            query.findInBackground(new FindCallback<ParseObject>() {
                @Override
                public void done(List<ParseObject> list, ParseException e) {
                    if (list.size() == 0) {
                        createCom();
                    } else {
                        Toast.makeText(CreateComunidadeActivity.this, "Por favor, utilize um nome de comunidade diferente", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }

    private boolean checkData() {
        boolean validationError = false;
        StringBuilder validationErrorMessage =
                new StringBuilder(getResources().getString(R.string.error_intro));
        if (isEmpty(comunidadeName.getEditText().getText().toString())) {
            validationError = true;
            validationErrorMessage.append(getResources().getString(R.string.error_blank_community_name));
        } else if (isEmpty(comunidadePassword.getEditText().getText().toString())) {
            validationError = true;
            validationErrorMessage.append(getResources().getString(R.string.error_blank_community_password));
        } else if (isEmpty(comunidadeConfirmPassword.getEditText().getText().toString())) {
            validationError = true;
            validationErrorMessage.append(getResources().getString(R.string.error_blank_community_confirm_password));
        } else if (!check_confirm_password()){
            validationError = true;
            validationErrorMessage.append(getResources().getString(R.string.error_mismatched_passwords));
        }
        validationErrorMessage.append(getResources().getString(R.string.error_end));
        // If there is a validation error, display the error
        if (validationError) {
            Toast.makeText(CreateComunidadeActivity.this, validationErrorMessage.toString(), Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    private boolean check_confirm_password() {
        return (comunidadePassword.getEditText().getText().toString().equals(comunidadeConfirmPassword.getEditText().getText().toString()));
    }

    private void createCom() {
        Comunidade novaComunidade = new Comunidade();
        novaComunidade.put("Nome", comunidadeName.getEditText().getText().toString());
        novaComunidade.put("Password", comunidadePassword.getEditText().getText().toString());
        novaComunidade.add("Users", ParseUser.getCurrentUser().getUsername().toString());
        novaComunidade.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    ParseUser currUser = ParseUser.getCurrentUser();
                    currUser.put("ComunidadeLogada", comunidadeName.getEditText().getText().toString());

                    currUser.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            Toast.makeText(CreateComunidadeActivity.this, "Comunidade criada com sucesso", Toast.LENGTH_LONG).show();
                            startActivity(new Intent(CreateComunidadeActivity.this, MainActivity_Comunidade.class));
                        }
                    });
                }
                else{
                    Toast.makeText(CreateComunidadeActivity.this, "Nome da comunidade já existe.", Toast.LENGTH_LONG).show();
                }
            }
        });

    }

    private boolean isEmpty(String text) {
        if (text.length() > 0) {
            return false;
        } else {
            return true;
        }
    }
}